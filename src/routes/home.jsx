import React from "react";
function Home(props){
    return(
        <main className="bg-red-100">
            <div className="m-auto container">
            <h1 className="text-4xl sm:text-5xl text-red-950 md:text-6xl font-bold px-5 py-10">Popular Products</h1>
            <div className="">
            <div className="grid grid-cols-1 sm:grid-cols-2  lg:grid-cols-3 xl:grid-cols-4 m-5 gap-10"> 
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                <article className="shadow-2xl">
                    <div className="shadow-inner p-3 bg-red-200 rounded-lg">
                    <img className="size-full rounded-md" src="https://th.bing.com/th/id/OIP.cECqNrbBnovGd59SkxlyrwHaHa?w=149&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7" alt="" />
                    <div className="productDetails">
                            <h3 className="text-2xl font-bold">My product name</h3>
                            <div>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9733;</span>
                                <span className="text-3xl text-yellow-400">&#9734; </span>
                            </div>
                            <div className="flex flex-row items-center justify-between py-2">
                                <span className="text-xl">MRP &#8377;<span className="font-bold">159</span></span>
                                <button className="shadow-inner bg-red-950 text-red-300 px-5 py-3 rounded-lg">Add to cart</button>
                            </div>
                        </div>
                    </div>
                </article>
                
            </div>
            </div>
            </div>
        </main>
    )
}
export default Home